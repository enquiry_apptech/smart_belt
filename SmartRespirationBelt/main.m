//
//  main.m
//  SmartRespirationBelt
//
//  Created by ENYAN HUANG on 5/15/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SRTAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SRTAppDelegate class]));
    }
}
