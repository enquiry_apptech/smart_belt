//
//  SRTUtility.m
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/10/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTUtility.h"
#import <CommonCrypto/CommonDigest.h>

@implementation SRTUtility

+ (NSString *)encryptPassword:(NSString *)oriPassword
{
    const char *cStr = [oriPassword UTF8String];
    unsigned char digest[CC_MD5_DIGEST_LENGTH];
    CC_MD5(cStr, (CC_LONG)strlen(cStr), digest);
    NSMutableString *result = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++) {
        [result appendFormat:@"%02x", digest[i]];
    }
    
    return result;
}

@end
