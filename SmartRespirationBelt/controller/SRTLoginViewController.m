//
//  SRTLoginViewController.m
//  SmartRespirationBelt
//
//  Created by ENYAN HUANG on 5/16/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTLoginViewController.h"
#import "SRTServerCommunicator.h"
#import "SRTCoreDataModel.h"
#import "SRTUtility.h"

@interface SRTLoginViewController () <SRTServerCommunicatorDelegate>

@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;

@property (nonatomic, getter = isAppJustLaunched) BOOL appJustLaunched;

@end

@implementation SRTLoginViewController

#pragma mark - VCLC

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(logout:) name:SRTLogoutNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(appLaunched:) name:UIApplicationDidFinishLaunchingNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:YES];
    [SRTServerCommunicator sharedServerCommunicator].delegate = self;
}

- (void)viewDidAppear:(BOOL)animated
{
    if (self.isAppJustLaunched) {
        self.appJustLaunched = NO;
        NSString *token = [[NSUserDefaults standardUserDefaults] stringForKey:@"token"];
        if (token) {
            [self.indicator startAnimating];
            [[SRTServerCommunicator sharedServerCommunicator] loginWithToken:token];
        }
    }
}

#pragma mark - Observe notification

- (void)logout:(NSNotification *)notification
{
    [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)appLaunched:(NSNotification *)notification
{
    self.appJustLaunched = YES;
}

#pragma mark - IBActions

- (IBAction)forgetPasswordPressed:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://54.251.251.94/srt/forget.php"]];
}

- (IBAction)signInPressed:(id)sender
{
    if ([self.emailTextField.text isEqualToString:@""] || [self.passwordTextField.text isEqualToString:@""]) {
        [[[UIAlertView alloc] initWithTitle:@"Empty Field" message:@"Email or password cannot be empty." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    [[SRTServerCommunicator sharedServerCommunicator] loginWithEmail:self.emailTextField.text andEncryptPassword:[SRTUtility encryptPassword:self.passwordTextField.text]];
    [self.indicator startAnimating];
}

#pragma mark - SRTServerCommunicatorDelegate methods

- (void)serverCommunicatorLoginSuccess:(SRTServerCommunicator *)communicator
{
    [self.indicator stopAnimating];
    [self performSegueWithIdentifier:@"modalToMain" sender:self];
}

- (void)serverCommunicator:(SRTServerCommunicator *)communicator loginFailWithMessage:(NSString *)message
{
    [self.indicator stopAnimating];

    [[[UIAlertView alloc] initWithTitle:@"Login Fail" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}
@end
