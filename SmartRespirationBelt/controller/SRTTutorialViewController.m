//
//  SRTTutorialViewController.m
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/6/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTTutorialViewController.h"

@interface SRTTutorialViewController () <UIScrollViewDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;

@end

@implementation SRTTutorialViewController

#pragma mark - VCLC

- (void)viewDidLoad
{
    [super viewDidLoad];
    UIImageView *content = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"fullTut"]];
    [self.scrollView addSubview:content];
}

#pragma mark - IBActions

- (IBAction)pageControlPressed:(id)sender
{
    UIPageControl *pageControl = sender;
    switch (pageControl.currentPage) {
        case 0:
            [self.scrollView setContentOffset:CGPointMake(0, 0) animated:YES];
            break;
        case 1:
            [self.scrollView setContentOffset:CGPointMake(320, 0) animated:YES];
            break;
        case 2:
            [self.scrollView setContentOffset:CGPointMake(640, 0) animated:YES];
            break;
        default:
            break;
    }
}

#pragma mark - UIUIScrollViewDelegate methods

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
{
    switch ((int)scrollView.contentOffset.x)
    {
        case 0:
            self.pageControl.currentPage = 0;
            break;
        case 320:
            self.pageControl.currentPage = 1;
            break;
        case 640:
            self.pageControl.currentPage = 2;
            break;
        default:
            break;
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    if (scrollView.contentOffset.x > 700) {
        [self.source dismissViewControllerAnimated:YES completion:NULL];
    }
}
@end
