//
//  SRTTimelineViewController.m
//  SmartRespirationBelt
//
//  Created by ENYAN HUANG on 5/16/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTTimelineViewController.h"
#import "SRTCoreDataModel.h"
#import "SRTTimelineTableViewCell.h"
#import "SRTServerCommunicator.h"

@interface SRTTimelineViewController () <NSFetchedResultsControllerDelegate, SRTServerCommunicatorDelegate, UIAlertViewDelegate>

@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBarButtonItem;

@end

@implementation SRTTimelineViewController

#pragma mark - Property getters and setters

-(NSFetchedResultsController *)fetchedResultsController
{
    if (!_fetchedResultsController) {
        NSManagedObjectContext *context = [SRTCoreDataModel sharedCoreDataModel].managedObjectContext;
        NSFetchRequest *request = [[NSFetchRequest alloc] init];
        NSEntityDescription *entity = [NSEntityDescription entityForName:@"SRTCoaching" inManagedObjectContext:context];
        request.entity = entity;
        NSSortDescriptor *sort = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
        request.sortDescriptors = @[sort];
        request.fetchBatchSize = 20;
        
        _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:request managedObjectContext:context sectionNameKeyPath:nil cacheName:nil];
        
        _fetchedResultsController.delegate = self;
    }
    
    return _fetchedResultsController;
    
}

#pragma mark - VCLC

- (void)viewDidLoad
{
    [self.fetchedResultsController performFetch:NULL];
    [SRTServerCommunicator sharedServerCommunicator].delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    space.width = -18;
    
    self.navigationItem.leftBarButtonItems = @[space, self.backBarButtonItem];
}
#pragma mark - IBActions

- (IBAction)syncPressed:(id)sender
{
    [[SRTServerCommunicator sharedServerCommunicator] uploadCoachingDataForUser:[SRTUser currentUser]];
    
    for (SRTCoaching *aCoaching in [SRTUser currentUser].userCoaching) {
        [[SRTCoreDataModel sharedCoreDataModel].managedObjectContext deleteObject:aCoaching];
    }
    
    [[SRTServerCommunicator sharedServerCommunicator] downloadCoachingDataForUser:[SRTUser currentUser]];
}

- (IBAction)clearPressed:(id)sender
{
    
    [[[UIAlertView alloc] initWithTitle:@"Clear confirmation" message:@"Are you sure to clear all the data?" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Confrim", nil] show];
}

- (IBAction)backPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [[self.fetchedResultsController.sections objectAtIndex:section] numberOfObjects];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TimelineCell" forIndexPath:indexPath];
    
    // Configure the cell...
    if ([cell isKindOfClass:[SRTTimelineTableViewCell class]]) {
        SRTTimelineTableViewCell *timelineCell = (SRTTimelineTableViewCell *)cell;
        timelineCell.coachingData = [self.fetchedResultsController objectAtIndexPath:indexPath];
    }
    
    return cell;
}

#pragma mark - NSFetchedResultsControllerDelegate methods

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath
{
    [self.tableView reloadData];
}

#pragma mark - SRTServerCommunicatorDelegate methods

- (void)serverCommunicator:(SRTServerCommunicator *)communicator downloadCoachingDataFailWithMessage:(NSString *)message
{
    [[[UIAlertView alloc] initWithTitle:@"Download fail" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void)serverCommunicator:(SRTServerCommunicator *)communicator uploadCoachingDataFailWithMessage:(NSString *)message
{
    [[[UIAlertView alloc] initWithTitle:@"Upload fail" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void)serverCommunicatorDownloadCoachingDataSuccess:(SRTServerCommunicator *)communicator
{
    [[[UIAlertView alloc] initWithTitle:@"Success" message:@"Synchronize data with server successfull!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

- (void)serverCommunicatorUploadCoachingDataSuccess:(SRTServerCommunicator *)communicator
{
    
}
#pragma mark - UIAlertViewDelegate methods

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == alertView.cancelButtonIndex) {
        return;
    } else {
        for (SRTCoaching *coaching in [self.fetchedResultsController fetchedObjects]) {
            [[SRTCoreDataModel sharedCoreDataModel].managedObjectContext deleteObject:coaching];
        }
        [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
