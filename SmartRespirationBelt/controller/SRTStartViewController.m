//
//  SRTStartViewController.m
//  SmartRespirationBelt
//
//  Created by ENYAN HUANG on 5/18/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTStartViewController.h"

@interface SRTStartViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end

@implementation SRTStartViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

#pragma mark - Page View Controller Data Source

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerBeforeViewController:(UIViewController *)viewController
{

}

-(UIViewController *)pageViewController:(UIPageViewController *)pageViewController viewControllerAfterViewController:(UIViewController *)viewController
{
    
}
@end
