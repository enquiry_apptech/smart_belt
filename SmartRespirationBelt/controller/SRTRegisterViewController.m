//
//  SRTRegisterViewController.m
//  SmartRespirationBelt
//
//  Created by ENYAN HUANG on 5/16/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTRegisterViewController.h"
#import "SRTCoreDataModel.h"
#import "SRTServerCommunicator.h"
#import "SRTUtility.h"

@interface SRTRegisterViewController () <SRTServerCommunicatorDelegate>
@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UILabel *maleLabel;
@property (weak, nonatomic) IBOutlet UILabel *femaleLabel;
@property (weak, nonatomic) IBOutlet UITextField *ageTextField;
@property (weak, nonatomic) IBOutlet UITextField *heightTextField;
@property (weak, nonatomic) IBOutlet UITextField *weightTextField;
@property (weak, nonatomic) IBOutlet UITextField *emailTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBarButtonItem;

@end

@implementation SRTRegisterViewController

#pragma mark - VCLC

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[self navigationController] setNavigationBarHidden:NO];
    [SRTServerCommunicator sharedServerCommunicator].delegate = self;
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    space.width = -18;
    
    self.navigationItem.leftBarButtonItems = @[space, self.backBarButtonItem];
}

#pragma mark - IBActions

- (IBAction)submitPressed:(id)sender
{
    if (![self.passwordTextField.text isEqualToString:self.confirmTextField.text]) {
        [[[UIAlertView alloc] initWithTitle:@"Password Mismatch" message:@"The password doesn't match!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    
    if ([self.nameTextField.text isEqualToString:@""] || [self.ageTextField.text isEqualToString:@""] || [self.weightTextField.text isEqualToString:@""] || [self.heightTextField.text isEqualToString:@""] || [self.emailTextField.text isEqualToString:@""] || [self.passwordTextField.text isEqualToString:@""]) {
        [[[UIAlertView alloc] initWithTitle:@"Empty Field" message:@"No field can be empty." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    NSManagedObjectContext *context = [SRTCoreDataModel sharedCoreDataModel].managedObjectContext;
    SRTUser *aUser = [NSEntityDescription insertNewObjectForEntityForName:@"SRTUser" inManagedObjectContext:context];
    aUser.name = self.nameTextField.text;
    if ([self.maleLabel.text isEqualToString:@"🔵Male"]) {
        aUser.gender = @"M";
    } else {
        aUser.gender = @"F";
    }
    aUser.age = [NSNumber numberWithInteger:[self.ageTextField.text integerValue]];
    aUser.height = [NSNumber numberWithFloat:[self.heightTextField.text floatValue]];
    aUser.weight = [NSNumber numberWithFloat:[self.weightTextField.text floatValue]];
    aUser.email = self.emailTextField.text;
    aUser.isCurrent = [NSNumber numberWithBool:NO];
    
    [self.indicator startAnimating];
    [[SRTServerCommunicator sharedServerCommunicator] registerUser:aUser withEncryptPassword:[SRTUtility encryptPassword:self.passwordTextField.text]];
        
}

- (IBAction)genderLabelPressed:(UITapGestureRecognizer *)sender
{
    if (sender.view == self.maleLabel) {
        self.maleLabel.text = @"🔵Male";
        self.femaleLabel.text = @"⚪️Female";
    } else if (sender.view == self.femaleLabel) {
        self.maleLabel.text = @"⚪️Male";
        self.femaleLabel.text = @"🔵Female";
    }
}

- (IBAction)backPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SRTServerCommunicatorDelegate methods

- (void)serverCommunicatorRegisterSuccess:(SRTServerCommunicator *)communicator
{
    [self.indicator stopAnimating];
    [[[UIAlertView alloc] initWithTitle:@"Register Success" message:@"Register user successfully, please log in" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)serverCommunicator:(SRTServerCommunicator *)communicator registerFailWithMessage:(NSString *)message
{
    [self.indicator stopAnimating];
    [[[UIAlertView alloc] initWithTitle:@"Register Fail" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

@end
