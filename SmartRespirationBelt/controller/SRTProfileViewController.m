//
//  SRTProfileViewController.m
//  SmartRespirationBelt
//
//  Created by ENYAN HUANG on 5/16/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTProfileViewController.h"
#import "SRTCoreDataModel.h"
#import "SRTServerCommunicator.h"
#import "SRTUtility.h"

@interface SRTProfileViewController () <SRTServerCommunicatorDelegate>

@property (weak, nonatomic) IBOutlet UITextField *nameTextField;
@property (weak, nonatomic) IBOutlet UILabel *maleLabel;
@property (weak, nonatomic) IBOutlet UILabel *femaleLabel;
@property (weak, nonatomic) IBOutlet UITextField *ageTextField;
@property (weak, nonatomic) IBOutlet UITextField *heightTextField;
@property (weak, nonatomic) IBOutlet UITextField *weightTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;
@property (weak, nonatomic) IBOutlet UITextField *confirmTextField;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBarButtonItem;

@end

@implementation SRTProfileViewController

#pragma mark - VCLC

- (void)viewWillAppear:(BOOL)animated
{
    [SRTServerCommunicator sharedServerCommunicator].delegate = self;
    [self updateUI];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    space.width = -18;
    
    self.navigationItem.leftBarButtonItems = @[space, self.backBarButtonItem];

}

#pragma mark - IBActions

- (IBAction)savePressed:(id)sender
{
    
    if ([self.nameTextField.text isEqualToString:@""] || [self.ageTextField.text isEqualToString:@""] || [self.weightTextField.text isEqualToString:@""] || [self.heightTextField.text isEqualToString:@""]) {
        [[[UIAlertView alloc] initWithTitle:@"Empty Field" message:@"No field can be empty." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        return;
    }
    SRTUser *currentUser = [SRTUser currentUser];
    currentUser.name = self.nameTextField.text;
    if ([self.maleLabel.text isEqualToString:@"🔵Male"]) {
        currentUser.gender = @"M";
    } else {
        currentUser.gender = @"F";
    }
    currentUser.age = [NSNumber numberWithInteger:[self.ageTextField.text integerValue]];
    currentUser.height = [NSNumber numberWithFloat:[self.heightTextField.text floatValue]];
    currentUser.weight = [NSNumber numberWithFloat:[self.weightTextField.text floatValue]];

    [self.indicator startAnimating];
    if (![self.passwordTextField.text isEqualToString:@""]) {
        if (![self.passwordTextField.text isEqualToString:self.confirmTextField.text]) {
            [[[UIAlertView alloc] initWithTitle:@"Password Mismatch" message:@"The password doesn't match!" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            return;
        }
        [[SRTServerCommunicator sharedServerCommunicator] updateProfileForUser:currentUser withEncryptPassword:[SRTUtility encryptPassword:self.passwordTextField.text]];
    } else {
        [[SRTServerCommunicator sharedServerCommunicator] updateProfileForUser:currentUser withEncryptPassword:@""];
    }
    
    
}

- (IBAction)cancelPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)genderLabelPressed:(UITapGestureRecognizer *)sender
{
    if (sender.view == self.maleLabel) {
        self.maleLabel.text = @"🔵Male";
        self.femaleLabel.text = @"⚪️Female";
    } else if (sender.view == self.femaleLabel) {
        self.maleLabel.text = @"⚪️Male";
        self.femaleLabel.text = @"🔵Female";
    }
}

- (IBAction)backPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SRTServerCommunicatorDelegate methods

- (void)serverCommunicatorUpdateProfileSuccess:(SRTServerCommunicator *)communicator
{
    [self.indicator stopAnimating];
    [[[UIAlertView alloc] initWithTitle:@"Update Success" message:@"Profile update successfully" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
    [self updateUI];
}

- (void)serverCommunicator:(SRTServerCommunicator *)communicator updateProfileFailWithMessage:(NSString *)message
{
    [self.indicator stopAnimating];
    [[[UIAlertView alloc] initWithTitle:@"Update Fail" message:message delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
}

#pragma mark - Helpers

- (void)updateUI
{
    SRTUser *currentUser = [SRTUser currentUser];
    self.nameTextField.text = currentUser.name;
    if ([currentUser.gender isEqualToString:@"M"]) {
        self.maleLabel.text = @"🔵Male";
        self.femaleLabel.text = @"⚪️Female";
    } else {
        self.maleLabel.text = @"⚪️Male";
        self.femaleLabel.text = @"🔵Female";
    }
    self.ageTextField.text = [NSString stringWithFormat:@"%@", currentUser.age];
    self.heightTextField.text = [NSString stringWithFormat:@"%@", currentUser.height];
    self.weightTextField.text = [NSString stringWithFormat:@"%@", currentUser.weight];
    self.passwordTextField.text = @"";
    self.confirmTextField.text = @"";
}

@end
