//
//  SRTSettingViewController.m
//  SmartRespirationBelt
//
//  Created by ENYAN HUANG on 5/16/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTSettingViewController.h"
#import "SRTBelt.h"
#import "SRTBeltDataManager.h"
#import "SRTCoreDataModel.h"
#import "SRTServerCommunicator.h"

@interface SRTSettingViewController () <SRTBeltDataManagerDelegate>

//Device part
@property (weak, nonatomic) IBOutlet UILabel *connectLabel;
@property (weak, nonatomic) IBOutlet UILabel *batteryLabel;
@property (weak, nonatomic) IBOutlet UIButton *connectBtn;

//Profile part
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *generLabel;
@property (weak, nonatomic) IBOutlet UILabel *ageLabel;
@property (weak, nonatomic) IBOutlet UILabel *heightLabel;
@property (weak, nonatomic) IBOutlet UILabel *weightLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBarButtonItem;

@end

@implementation SRTSettingViewController

#pragma mark - VCLC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(disconnected:) name:SRTDisconnectBeltNotification object:nil];
}

- (void)viewWillAppear:(BOOL)animated
{
    [SRTBeltDataManager sharedBeltDataManager].delegate = self;
    if ([[SRTBelt sharedBelt] isConnected]) {
        [[SRTBelt sharedBelt] readBattery];
        self.connectLabel.text = @"YES";
        self.batteryLabel.text = @"loading";
        [self.connectBtn setTitle:@"DISCONNECT" forState:UIControlStateNormal];
        [self.connectBtn removeTarget:self action:@selector(connect) forControlEvents:UIControlEventTouchUpInside];
        [self.connectBtn addTarget:self action:@selector(disconnect) forControlEvents:UIControlEventTouchUpInside];
    } else {
        self.connectLabel.text = @"NO";
        self.batteryLabel.text = @"N/A";
        [self.connectBtn setTitle:@"CONNECT" forState:UIControlStateNormal];
        [self.connectBtn removeTarget:self action:@selector(disconnect) forControlEvents:UIControlEventTouchUpInside];
        [self.connectBtn addTarget:self action:@selector(connect) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    SRTUser *currentUser = [SRTUser currentUser];
    self.nameLabel.text = currentUser.name;
    if ([currentUser.gender isEqualToString:@"M"]) {
        self.generLabel.text = @"Male";
    } else {
        self.generLabel.text = @"Female";
    }
    self.ageLabel.text = [NSString stringWithFormat:@"%@", currentUser.age];
    self.heightLabel.text = [NSString stringWithFormat:@"%@ cm", currentUser.height];
    self.weightLabel.text = [NSString stringWithFormat:@"%@ kg", currentUser.weight];
    self.emailLabel.text = currentUser.email;
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    space.width = -18;
    
    self.navigationItem.leftBarButtonItems = @[space, self.backBarButtonItem];
}

#pragma mark - Observe notifications

- (void)disconnected:(NSNotification *)notification
{
    self.connectLabel.text = @"NO";
    self.batteryLabel.text = @"N/A";
    [self.connectBtn setTitle:@"CONNECT" forState:UIControlStateNormal];
    [self.connectBtn removeTarget:self action:@selector(disconnect) forControlEvents:UIControlEventTouchUpInside];
    [self.connectBtn addTarget:self action:@selector(connect) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - Target actions

- (void)connect
{
    [self performSegueWithIdentifier:@"pushToFindDevice" sender:self];
}

- (void)disconnect
{
    [[SRTBelt sharedBelt] disconnect];
}

#pragma mark - IBActions

- (IBAction)logoutPressed:(id)sender
{
    [[SRTServerCommunicator sharedServerCommunicator] logout];
}

- (IBAction)backPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SRTBeltDataManagerDelegate methods

-(void)beltDataManager:(SRTBeltDataManager *)manager didUpdateBattery:(NSUInteger)battery
{
    self.batteryLabel.text = [NSString stringWithFormat:@"%lu%%", (unsigned long)battery];
}

@end
