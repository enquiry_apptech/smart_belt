//
//  SRTCoachingViewController.m
//  SmartRespirationBelt
//
//  Created by ENYAN HUANG on 5/16/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTCoachingSetViewController.h"
#import "SRTCoachingProgressViewController.h"
#import "SRTBelt.h"
#import "SRTTutorialViewController.h"

@interface SRTCoachingSetViewController () <UIActionSheetDelegate>

@property (nonatomic) NSTimeInterval trainingLength;
@property (nonatomic) NSString *trainingLevel;

@property (weak, nonatomic) IBOutlet UIButton *timerSelectionBtn;
@property (weak, nonatomic) IBOutlet UIButton *relaxationLevelBtn;
@property (weak, nonatomic) IBOutlet UISwitch *voiceReminderSwitch;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *reportBarButtonItem;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *settingBarButtonItem;

@end

@implementation SRTCoachingSetViewController

#pragma mark - Property getters and setter

- (NSString *)trainingLevel
{
    if (!_trainingLevel) {
        _trainingLevel = @"general";
    }
    
    return _trainingLevel;
}

#pragma mark - VCLC

- (void)viewDidLoad
{
    self.trainingLength = 15 * 60;
    self.trainingLevel = 0;
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //[[[self tabBarController] navigationItem] setTitle:@"Coaching"];
    UIBarButtonItem *space1 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    space1.width = -18;
    
    UIBarButtonItem *space2 = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    space2.width = -18;
    
    self.navigationItem.leftBarButtonItems = @[space1, self.reportBarButtonItem];
    self.navigationItem.rightBarButtonItems = @[space2, self.settingBarButtonItem];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:YES];
    BOOL launched = [[NSUserDefaults standardUserDefaults] boolForKey:@"launched"];
    if (!launched) {
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"launched"];
        [self performSegueWithIdentifier:@"modalToTut" sender:self];
    }
}

#pragma mark - IBActions

- (IBAction)timerSelectionPressed:(id)sender
{
    UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:@"Select Coaching Length" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"5 mins", @"15 mins", @"30 mins", @"1 hr", nil];
    [as showInView:self.view];
}

- (IBAction)relaxationLevelSelectionPressed:(id)sender
{
    UIActionSheet *as = [[UIActionSheet alloc] initWithTitle:@"Select Relaxation Level" delegate:self cancelButtonTitle:nil destructiveButtonTitle:nil otherButtonTitles:@"general", nil];
    [as showInView:self.view];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender
{
    if ([identifier isEqualToString:@"pushToProgress"]) {
        if (![[SRTBelt sharedBelt] isConnected]) {
            [[[UIAlertView alloc] initWithTitle:@"Belt not connectd" message:@"No belt is connected, please go to setting page to connect" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
            return false;
        }
    }
    
    return true;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    if ([segue.identifier isEqualToString:@"pushToProgress"]) {
        SRTCoachingProgressViewController *dvc = (SRTCoachingProgressViewController *)[segue destinationViewController];
        dvc.trainingLength = self.trainingLength;
        dvc.trainingLevel = self.trainingLevel;
    } else if ([segue.identifier isEqualToString:@"modalToTut"]) {
        SRTTutorialViewController *dvc = segue.destinationViewController;
        dvc.source = self;
    }
}

#pragma mark - UIActionSheetDelegate methods
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if ([actionSheet.title isEqualToString:@"Select Coaching Length"]) {
        switch (buttonIndex) {
            case 0:
                self.trainingLength = 5 * 60;
                [self.timerSelectionBtn setTitle:@"5 mins" forState:UIControlStateNormal];
                break;
            case 1:
                self.trainingLength = 15 * 60;
                [self.timerSelectionBtn setTitle:@"15 mins" forState:UIControlStateNormal];
                break;
            case 2:
                self.trainingLength = 30 * 60;
                [self.timerSelectionBtn setTitle:@"30 mins" forState:UIControlStateNormal];
                break;
            case 3:
                self.trainingLength = 60 * 60;
                [self.timerSelectionBtn setTitle:@"1 hour" forState:UIControlStateNormal];
                break;
            default:
                break;
        }
    } else if ([actionSheet.title isEqualToString:@"Selection Relaxation Level"]) {
        switch (buttonIndex) {
            case 0:
                self.trainingLevel = @"general";
                [self.relaxationLevelBtn setTitle:@"general" forState:UIControlStateNormal];
                break;
            default:
                break;
        }
    }
}
@end
