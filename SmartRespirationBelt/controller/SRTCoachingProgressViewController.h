//
//  SRTCoachingProgressViewController.h
//  SmartRespirationBelt
//
//  Created by ENYAN HUANG on 5/16/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRTCoachingProgressViewController : UIViewController

@property (nonatomic) NSTimeInterval trainingLength;
@property (nonatomic, strong) NSString *trainingLevel;

@end
