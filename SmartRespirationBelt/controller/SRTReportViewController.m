//
//  SRTReportViewController.m
//  SmartRespirationBelt
//
//  Created by ENYAN HUANG on 5/16/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTReportViewController.h"

@interface SRTReportViewController ()
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBarButtonItem;

@end

@implementation SRTReportViewController

#pragma mark - VCLC

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    space.width = -18;
    
    self.navigationItem.leftBarButtonItems = @[space, self.backBarButtonItem];
}

- (IBAction)backPressed:(id)sender
{
    [[self navigationController] popToRootViewControllerAnimated:YES];
}
@end
