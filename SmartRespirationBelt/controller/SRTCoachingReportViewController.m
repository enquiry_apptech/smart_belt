//
//  SRTCoachingReportViewController.m
//  SmartRespirationBelt
//
//  Created by ENYAN HUANG on 5/16/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTCoachingReportViewController.h"
#import "SRTGraphView.h"
#import "SRTDataAnalyser.h"
#import "SRTCoreDataModel.h"

@interface SRTCoachingReportViewController () <SRTDataAnalyserDelegate>

@property (weak, nonatomic) IBOutlet SRTGraphView *graphView;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;
@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *totalTrainingTime;
@property (weak, nonatomic) IBOutlet UILabel *breathRateLabel;
@property (weak, nonatomic) IBOutlet UILabel *breathDepthLabel;

@end

@implementation SRTCoachingReportViewController

#pragma mark - VCLC

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    //Only draw the points in the first 30 seconds
    self.graphView.points = [self.beltData subarrayWithRange:NSMakeRange(0, [self.beltData count] > 30 ? 30 : [self.beltData count])];
    
    SRTDataAnalyser *analyser = [[SRTDataAnalyser alloc] init];
    analyser.delegate = self;
    analyser.beltData = self.beltData;
    
    [analyser startAnalysing];
    
    self.dateLabel.text = [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterMediumStyle timeStyle:NSDateFormatterNoStyle];
    self.timeLabel.text = [NSDateFormatter localizedStringFromDate:[NSDate date] dateStyle:NSDateFormatterNoStyle timeStyle:NSDateFormatterMediumStyle];
    self.totalTrainingTime.text = [NSString stringWithFormat:@"%d min %d sec", (int)self.trainingTime / 60, (int)self.trainingTime % 60];
    
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    space.width = -18;

}

#pragma mark - SRTDataAnalyserDelegate methods

- (void)dataAnalyser:(SRTDataAnalyser *)analyser averageBreathRate:(float)rate averageBreathDepth:(float)depth;
{
    self.breathRateLabel.text = [NSString stringWithFormat:@"%.1f", rate];
    
    if (depth > 0) {
        self.breathDepthLabel.text = @"Low";
    }
    if (depth > 50) {
        self.breathDepthLabel.text = @"Medium";
    }
    if (depth > 100) {
        self.breathDepthLabel.text = @"high";
    }
    
    SRTCoaching *aCoaching = [NSEntityDescription insertNewObjectForEntityForName:@"SRTCoaching" inManagedObjectContext:[SRTCoreDataModel sharedCoreDataModel].managedObjectContext];
    aCoaching.date = [NSDate date];
    aCoaching.relaxationLevel = self.trainingLevel;
    aCoaching.rate = [NSNumber numberWithFloat:rate];
    aCoaching.depth = [NSNumber numberWithFloat:depth];
    aCoaching.length = [NSNumber numberWithDouble:self.trainingTime];
    aCoaching.coachingUser = [SRTUser currentUser];
    aCoaching.uploaded = [NSNumber numberWithBool:NO];
}
@end
