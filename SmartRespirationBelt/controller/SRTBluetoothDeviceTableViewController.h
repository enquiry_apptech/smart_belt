//
//  SRTBluetoothDeviceTableViewController.h
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/3/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRTBluetoothDeviceTableViewController : UITableViewController

@end
