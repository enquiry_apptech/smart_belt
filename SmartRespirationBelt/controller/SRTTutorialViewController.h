//
//  SRTTutorialViewController.h
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/6/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRTTutorialViewController : UIViewController

@property (weak, nonatomic) UIViewController *source;

@end
