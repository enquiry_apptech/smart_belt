//
//  SRTExerciseViewController.m
//  SmartRespirationBelt
//
//  Created by ENYAN HUANG on 5/16/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTExerciseSetViewController.h"

@interface SRTExerciseSetViewController ()

@end

@implementation SRTExerciseSetViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[[self tabBarController] navigationItem] setTitle:@"Exercise"];
}

@end
