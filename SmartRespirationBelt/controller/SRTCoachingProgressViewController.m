//
//  SRTCoachingProgressViewController.m
//  SmartRespirationBelt
//
//  Created by ENYAN HUANG on 5/16/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTCoachingProgressViewController.h"
#import "SRTBelt.h"
#import "SRTBeltDataManager.h"
#import "SRTCoachingReportViewController.h"
#import <math.h>

@interface SRTCoachingProgressViewController () <SRTBeltDataManagerDelegate>

@property (strong, nonatomic) NSTimer *timer;
@property (nonatomic) NSTimeInterval timePast;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *lengthLabel;
@property (weak, nonatomic) IBOutlet UITextField *timerTextField;
@property (strong, nonatomic) NSMutableArray *beltData; //of NSNumber
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBarButtonItem;

@end

@implementation SRTCoachingProgressViewController

#pragma mark - Property getters and setters

- (NSMutableArray *)beltData
{
    if (!_beltData) {
        _beltData = [[NSMutableArray alloc] init];
    }
    
    return _beltData;
}

#pragma mark - VCLC

- (void)viewDidLoad
{
    [self.navigationItem setHidesBackButton:YES];
    [SRTBeltDataManager sharedBeltDataManager].delegate = self;

}

- (void)viewWillAppear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(lostConnection:) name:SRTDisconnectBeltNotification object:nil];
    self.levelLabel.text = [self.trainingLevel uppercaseString];
    self.lengthLabel.text = [NSString stringWithFormat:@"%d mins", (int)self.trainingLength / 60];
    
    self.timer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(checkTime:) userInfo:nil repeats:YES];
    
    [[SRTBelt sharedBelt] startUpdateResister];
    [self updateUI];

}

- (void)viewWillDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SRTDisconnectBeltNotification object:nil];
    [[SRTBelt sharedBelt] stopUpdateResister];
}

#pragma mark - Observe notification

- (void)lostConnection:(NSNotification *)notification
{
    [[[UIAlertView alloc] initWithTitle:@"Connection Lost" message:@"Lose connection with the belt." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - IBActions

- (IBAction)cancelPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Target actions

- (void)checkTime:(id)sender
{
    self.timePast += 1;
    if (self.timePast >= self.trainingLength) {
        [self.timer invalidate];
        [self performSegueWithIdentifier:@"pushToReport" sender:self.timer];
    } else {
        [self updateUI];
    }
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    if ([segue.identifier isEqualToString:@"pushToReport"]) {
        SRTCoachingReportViewController *dvc = (SRTCoachingReportViewController *)segue.destinationViewController;
        dvc.beltData = self.beltData;
        if (sender == self.timer) {
            dvc.trainingTime = self.trainingLength;
        } else {
            dvc.trainingTime = self.timePast;
        }
        
        dvc.trainingLevel = self.trainingLevel;
    }
}

#pragma mark - SRTBeltDataManagerDelegate methods

- (void)beltDataManager:(SRTBeltDataManager *)manager didUpdateResister:(double)resisterInOhm
{
    NSLog(@"get value: %f", resisterInOhm);
    [self.beltData addObject:[NSNumber numberWithDouble:resisterInOhm]];
}

#pragma mark - Helpers

- (void)updateUI
{
    NSTimeInterval timeLeft = self.trainingLength - self.timePast;
    int minute = (int)timeLeft / 60;
    int second = (int)timeLeft % 60;
    self.timerTextField.text = [NSString stringWithFormat:@"%02d : %02d", minute, second];
}
@end
