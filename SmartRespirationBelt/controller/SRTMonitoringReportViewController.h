//
//  SRTMonitoringReportViewController.h
//  SmartRespirationBelt
//
//  Created by ENYAN HUANG on 5/16/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SRTReportViewController.h"

@interface SRTMonitoringReportViewController : SRTReportViewController

@end
