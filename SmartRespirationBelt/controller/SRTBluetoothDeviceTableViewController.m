//
//  SRTBluetoothDeviceTableViewController.m
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/3/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTBluetoothDeviceTableViewController.h"
#import "SRTBelt.h"

@interface SRTBluetoothDeviceTableViewController ()

@property (nonatomic, strong) NSMutableArray *devices;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *indicator;
@property (strong, nonatomic) NSTimer *timer;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBarButtonItem;

@end

@implementation SRTBluetoothDeviceTableViewController

#pragma mark - Property getters and setters

- (NSMutableArray *)devices
{
    if (!_devices) {
        _devices = [[NSMutableArray alloc] init];
    }
    
    return _devices;
}

#pragma mark - VCLC

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(aDeviceFound:) name:SRTDiscoverBeltNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(beltConnected:) name:SRTConnectBeltNotification object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(unrecognizedDevice:) name:SRTUnrecognizedDeviceNotification object:nil];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    UIBarButtonItem *space = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFixedSpace target:nil action:nil];
    space.width = -18;
    
    self.navigationItem.leftBarButtonItems = @[space, self.backBarButtonItem];

}
- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    if ([SRTBelt sharedBelt].beltManagerState != CBCentralManagerStatePoweredOn) {
        [[[UIAlertView alloc] initWithTitle:@"Bluetooth Unavailable" message:@"The Bluetooth is currently unavailable for the device" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
        [self.navigationController popViewControllerAnimated:YES];
    }
    [[SRTBelt sharedBelt] startScan];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    if ([SRTBelt sharedBelt].beltManagerState == CBCentralManagerStatePoweredOn) {
        [[SRTBelt sharedBelt] stopScan];
    }
    [super viewWillDisappear:animated];
}

#pragma mark - Observe notifications

- (void)aDeviceFound:(NSNotification *)notification
{
    CBPeripheral *device = [notification.userInfo valueForKey:@"peripheral"];
    if (![device.name isEqualToString:@""]) {
        [self.devices addObject:device];
    }
    
    [self.tableView reloadData];
}

- (void)beltConnected:(NSNotification *)notification
{
    [[SRTBelt sharedBelt] stopScan];
    [self.timer invalidate];
    [self.indicator stopAnimating];
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)unrecognizedDevice:(NSNotification *)notification
{
    [self.timer invalidate];
    [self.indicator stopAnimating];
    [[[UIAlertView alloc] initWithTitle:@"Unsupported Device" message:@"The selected device is not supported." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    [self reloadDevices];
}
#pragma mark - IBActions

- (IBAction)backPressed:(id)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Target actions

- (void)timeout
{
    [[[UIAlertView alloc] initWithTitle:@"Timeout" message:@"connection timeout, please try again" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
    [self.indicator stopAnimating];
    [self reloadDevices];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [self.devices count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BluetoothDeviceCell" forIndexPath:indexPath];
    
    // Configure the cell...
    CBPeripheral *device = [self.devices objectAtIndex:indexPath.row];
    cell.textLabel.text = device.name;
    
    return cell;
}

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section
{
    return @"Scanning Bluetooth device.";
}

#pragma mark - UITableViewDelegate methods

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    CBPeripheral *device = [self.devices objectAtIndex:indexPath.row];
    [[SRTBelt sharedBelt] connectToPeripheral:device];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self.indicator startAnimating];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:10 target:self selector:@selector(timeout) userInfo:nil repeats:NO];
}

#pragma mark - Helpers

- (void)reloadDevices
{
    [self.devices removeAllObjects];
    [[SRTBelt sharedBelt] stopScan];
    [[SRTBelt sharedBelt] startScan];
}

@end
