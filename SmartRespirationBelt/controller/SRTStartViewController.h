//
//  SRTStartViewController.h
//  SmartRespirationBelt
//
//  Created by ENYAN HUANG on 5/18/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRTStartViewController : UIViewController <UIPageViewControllerDataSource>

@property (nonatomic) NSInteger pageIndex;

@end
