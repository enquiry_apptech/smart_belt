//
//  SRTCoreDataModel.h
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/4/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "SRTCoaching.h"
#import "SRTUser.h"
#import "SRTUser+UserMethod.h"

@interface SRTCoreDataModel : NSObject

@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
@property (readonly, strong, nonatomic) NSManagedObjectModel *managedObjectModel;
@property (readonly, strong, nonatomic) NSPersistentStoreCoordinator *persistentStoreCoordinator;

+ (instancetype)sharedCoreDataModel;
- (void)saveContext;
- (NSURL *)applicationDocumentsDirectory;

@end
