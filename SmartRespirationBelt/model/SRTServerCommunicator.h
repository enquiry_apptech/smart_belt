//
//  SRTServerCommunicator.h
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/4/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const SRTLogoutNotification;

@class SRTUser;
@protocol SRTServerCommunicatorDelegate;

@interface SRTServerCommunicator : NSObject

@property (nonatomic, weak) id<SRTServerCommunicatorDelegate> delegate;

+ (instancetype)sharedServerCommunicator;
- (void)registerUser:(SRTUser *)aUser withEncryptPassword:(NSString *)encryptPassword;
- (void)loginWithEmail:(NSString *)email andEncryptPassword:(NSString *)encryptPassword;
- (void)loginWithToken:(NSString *)token;
- (void)uploadCoachingDataForUser:(SRTUser *)user;
- (void)downloadCoachingDataForUser:(SRTUser *)user;
- (void)updateProfileForUser:(SRTUser *)user withEncryptPassword:(NSString *)encryptPassword;
- (void)logout;

@end

@protocol SRTServerCommunicatorDelegate <NSObject>

@optional
- (void)serverCommunicatorRegisterSuccess:(SRTServerCommunicator *)communicator;
- (void)serverCommunicator:(SRTServerCommunicator *)communicator registerFailWithMessage:(NSString *)message;
- (void)serverCommunicatorLoginSuccess:(SRTServerCommunicator *)communicator;
- (void)serverCommunicator:(SRTServerCommunicator *)communicator loginFailWithMessage:(NSString *)message;
- (void)serverCommunicatorUploadCoachingDataSuccess:(SRTServerCommunicator *)communicator;
- (void)serverCommunicator:(SRTServerCommunicator *)communicator uploadCoachingDataFailWithMessage:(NSString *)message;
- (void)serverCommunicatorUpdateProfileSuccess:(SRTServerCommunicator *)communicator;
- (void)serverCommunicator:(SRTServerCommunicator *)communicator updateProfileFailWithMessage:(NSString *)message;
- (void)serverCommunicatorDownloadCoachingDataSuccess:(SRTServerCommunicator *)communicator;
- (void)serverCommunicator:(SRTServerCommunicator *)communicator downloadCoachingDataFailWithMessage:(NSString *)message;
@end