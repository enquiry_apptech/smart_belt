//
//  SRTUser.m
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/4/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTUser.h"
#import "SRTCoaching.h"


@implementation SRTUser

@dynamic name;
@dynamic gender;
@dynamic age;
@dynamic height;
@dynamic weight;
@dynamic email;
@dynamic isCurrent;
@dynamic userCoaching;

@end
