//
//  SRTBeltDataManager.m
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 5/29/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTBeltDataManager.h"
#import "SRTBelt.h"

@implementation SRTBeltDataManager

#pragma mark - Override

- (void)dealloc
{
    [[SRTBelt sharedBelt] removeObserver:self forKeyPath:@"battery"];
    [[SRTBelt sharedBelt] removeObserver:self forKeyPath:@"resisterValues"];
}

#pragma mark - Interface implementation

+ (instancetype)sharedBeltDataManager
{
    static SRTBeltDataManager *singleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singleton = [[SRTBeltDataManager alloc] init];
        //Do your custom initialisation
        [[SRTBelt sharedBelt] addObserver:singleton forKeyPath:@"battery" options:NSKeyValueObservingOptionNew context:NULL];
        [[SRTBelt sharedBelt] addObserver:singleton forKeyPath:@"resisterValues" options:NSKeyValueObservingOptionNew context:NULL];
    });
    
    return singleton;
}

#pragma mark - KVO

- (void)observeValueForKeyPath:(NSString *)keyPath ofObject:(id)object change:(NSDictionary *)change context:(void *)context
{
    if ([keyPath isEqual: @"battery"]) {
        NSUInteger battery = [[change valueForKey:NSKeyValueChangeNewKey] unsignedIntegerValue];
        self.lastBattery = battery;
        [self.delegate beltDataManager:self didUpdateBattery:battery];
    } else if ([keyPath isEqualToString:@"resisterValues"]) {
        
        NSArray *value = [change valueForKey:NSKeyValueChangeNewKey];
        double average = [self averageOfArray:value];
        double rInOhm = average * 8;
        self.lastResisterInOhm = rInOhm;
        [self.delegate beltDataManager:self didUpdateResister:rInOhm];
    }
}

#pragma mark - Helpers

- (double)averageOfArray:(NSArray *)array
{
    double sum = 0.0;
    for (NSNumber *n in array) {
        sum += [n unsignedShortValue];
    }
    double average = sum / [array count];
    return average;
}

@end
