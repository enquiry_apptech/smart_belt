//
//  SRTUser+UserMethod.h
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/4/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTUser.h"

@interface SRTUser (UserMethod)

+ (SRTUser *)currentUser;

@end
