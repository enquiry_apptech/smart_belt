//
//  SRTBeltDataManager.h
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 5/29/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@protocol SRTBeltDataManagerDelegate;
@class SRTBelt;

@interface SRTBeltDataManager : NSObject

@property (weak, nonatomic) id<SRTBeltDataManagerDelegate> delegate;
@property (nonatomic) NSUInteger lastBattery;
@property (nonatomic) NSUInteger lastResisterInOhm;

+ (instancetype)sharedBeltDataManager;

@end

@protocol SRTBeltDataManagerDelegate <NSObject>

@optional
- (void)beltDataManager:(SRTBeltDataManager *)manager didUpdateBattery:(NSUInteger)battery;
- (void)beltDataManager:(SRTBeltDataManager *)manager didUpdateResister:(double)resisterInOhm;

@end