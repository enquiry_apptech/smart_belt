//
//  SRTCoaching.h
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/6/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SRTUser;

@interface SRTCoaching : NSManagedObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSNumber * depth;
@property (nonatomic, retain) NSNumber * length;
@property (nonatomic, retain) NSNumber * rate;
@property (nonatomic, retain) NSString * relaxationLevel;
@property (nonatomic, retain) NSNumber * uploaded;
@property (nonatomic, retain) SRTUser *coachingUser;

@end
