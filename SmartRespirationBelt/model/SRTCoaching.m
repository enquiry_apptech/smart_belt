//
//  SRTCoaching.m
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/6/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTCoaching.h"
#import "SRTUser.h"


@implementation SRTCoaching

@dynamic date;
@dynamic depth;
@dynamic length;
@dynamic rate;
@dynamic relaxationLevel;
@dynamic uploaded;
@dynamic coachingUser;

@end
