//
//  SRTUser.h
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/4/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class SRTCoaching;

@interface SRTUser : NSManagedObject

@property (nonatomic, retain) NSString * name;
@property (nonatomic, retain) NSString * gender;
@property (nonatomic, retain) NSNumber * age;
@property (nonatomic, retain) NSNumber * height;
@property (nonatomic, retain) NSNumber * weight;
@property (nonatomic, retain) NSString * email;
@property (nonatomic, retain) NSNumber * isCurrent;
@property (nonatomic, retain) NSOrderedSet *userCoaching;
@end

@interface SRTUser (CoreDataGeneratedAccessors)

- (void)insertObject:(SRTCoaching *)value inUserCoachingAtIndex:(NSUInteger)idx;
- (void)removeObjectFromUserCoachingAtIndex:(NSUInteger)idx;
- (void)insertUserCoaching:(NSArray *)value atIndexes:(NSIndexSet *)indexes;
- (void)removeUserCoachingAtIndexes:(NSIndexSet *)indexes;
- (void)replaceObjectInUserCoachingAtIndex:(NSUInteger)idx withObject:(SRTCoaching *)value;
- (void)replaceUserCoachingAtIndexes:(NSIndexSet *)indexes withUserCoaching:(NSArray *)values;
- (void)addUserCoachingObject:(SRTCoaching *)value;
- (void)removeUserCoachingObject:(SRTCoaching *)value;
- (void)addUserCoaching:(NSOrderedSet *)values;
- (void)removeUserCoaching:(NSOrderedSet *)values;
@end
