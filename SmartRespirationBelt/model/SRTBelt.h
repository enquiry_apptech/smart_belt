//
//  SRTBelt.h
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 5/28/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

extern NSString * const SRTBluetoothFailNotification;
extern NSString * const SRTDiscoverBeltNotification;
extern NSString * const SRTConnectBeltNotification;
extern NSString * const SRTUnrecognizedDeviceNotification;
extern NSString * const SRTDisconnectBeltNotification;

@interface SRTBelt : NSObject

@property (nonatomic, getter = isConnected) BOOL connected;
@property (nonatomic, readonly) NSNumber *battery;
@property (strong, nonatomic, readonly) NSArray *resisterValues;
@property (nonatomic) CBCentralManagerState beltManagerState;

+ (instancetype)sharedBelt;

- (void)startScan;
- (void)stopScan;

- (void)connectToPeripheral:(CBPeripheral *)peripheral;
- (void)disconnect;

- (void)readBattery;
- (void)startUpdateResister;
- (void)stopUpdateResister;

@end
