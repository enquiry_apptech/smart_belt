//
//  SRTDataAnalyser.h
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/3/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol SRTDataAnalyserDelegate;

@interface SRTDataAnalyser : NSObject

@property (nonatomic, strong) NSArray *beltData;
@property (nonatomic, weak) id<SRTDataAnalyserDelegate> delegate;
- (void)startAnalysing;

@end

@protocol SRTDataAnalyserDelegate <NSObject>

@optional
- (void)dataAnalyser:(SRTDataAnalyser *)analyser averageBreathRate:(float)rate averageBreathDepth:(float)depth;

@end