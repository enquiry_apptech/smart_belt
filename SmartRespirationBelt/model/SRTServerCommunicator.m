//
//  SRTServerCommunicator.m
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/4/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTServerCommunicator.h"
#import "SRTCoreDataModel.h"
#import <AFNetworking/AFNetworking.h>

NSString * const SRTLogoutNotification = @"SRTLogoutNotification";

@interface SRTServerCommunicator ()

@property (strong, nonatomic) AFHTTPRequestOperationManager *requestManager;
@property (strong, nonatomic) NSString *requestURLString;
@property (strong, nonatomic) NSString *servicePortal;

@end
@implementation SRTServerCommunicator

#pragma mark - Property getters and setters

- (AFHTTPRequestOperationManager *)requestManager
{
    if (!_requestManager) {
        _requestManager = [AFHTTPRequestOperationManager manager];
        _requestManager.requestSerializer = [AFHTTPRequestSerializer serializer];
        _requestManager.responseSerializer = [AFJSONResponseSerializer serializer];
    }
    
    return _requestManager;
}

- (NSString *)requestURLString
{
    return @"http://54.251.251.94/srt/login.php";
}

#pragma mark - Interface implementation

+ (instancetype)sharedServerCommunicator
{
    static SRTServerCommunicator *singleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singleton = [[SRTServerCommunicator alloc] init];
    });
    
    return singleton;
}

- (void)registerUser:(SRTUser *)aUser withEncryptPassword:(NSString *)encryptPassword
{
    NSDictionary *parameter = @{@"t": @2,
                                @"email": aUser.email,
                                @"name": aUser.name,
                                @"gender": aUser.gender,
                                @"weight": aUser.weight,
                                @"height": aUser.height,
                                @"age": aUser.age,
                                @"password": encryptPassword};
    
    [self.requestManager POST:self.requestURLString parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *response = responseObject;
        if (![[response valueForKey:@"code"] isEqualToNumber:@200]) {
            [self.delegate serverCommunicator:self registerFailWithMessage:[response valueForKey:@"message"]];
        } else {
            [self.delegate serverCommunicatorRegisterSuccess:self];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate serverCommunicator:self registerFailWithMessage:@"Error with the connection"];
    }];
}

- (void)loginWithEmail:(NSString *)email andEncryptPassword:(NSString *)encryptPassword
{
    NSDictionary *parameter = @{@"t": @0,
                                @"email": email,
                                @"password": encryptPassword};
    [self.requestManager POST:self.requestURLString parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *userInfo = responseObject;
        
        if (![[userInfo valueForKey:@"code"] isEqualToNumber:@200]) {
            [self.delegate serverCommunicator:self loginFailWithMessage:[userInfo valueForKey:@"message"]];
        } else {
            NSManagedObjectContext *context = [SRTCoreDataModel sharedCoreDataModel].managedObjectContext;
            SRTUser *aUser = [NSEntityDescription insertNewObjectForEntityForName:@"SRTUser" inManagedObjectContext:context];
            aUser.name = [userInfo valueForKey:@"name"];
            aUser.gender = [userInfo valueForKey:@"gender"];
            aUser.age = [userInfo valueForKey:@"age"];
            aUser.weight = [userInfo valueForKey:@"weight"];
            aUser.height = [userInfo valueForKey:@"height"];
            aUser.email = [userInfo valueForKey:@"email"];
            aUser.isCurrent = [NSNumber numberWithBool:YES];
            
            [[NSUserDefaults standardUserDefaults] setObject:[userInfo valueForKey:@"token"] forKey:@"token"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [self.delegate serverCommunicatorLoginSuccess:self];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate serverCommunicator:self loginFailWithMessage:@"Error with the connection"];
    }];

}

- (void)loginWithToken:(NSString *)token
{
    NSDictionary *parameter = @{@"t": @1,
                                @"token": token};
    [self.requestManager POST:self.requestURLString parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *userInfo = responseObject;
        
        if (![[userInfo valueForKey:@"code"] isEqualToNumber:@200]) {
            [self.delegate serverCommunicator:self loginFailWithMessage:[userInfo valueForKey:@"message"]];
        } else {
            SRTUser *aUser = [SRTUser currentUser];
            if (!aUser) {
                NSManagedObjectContext *context = [SRTCoreDataModel sharedCoreDataModel].managedObjectContext;
                aUser = [NSEntityDescription insertNewObjectForEntityForName:@"SRTUser" inManagedObjectContext:context];
                aUser.isCurrent = [NSNumber numberWithBool:YES];
            }
            aUser.name = [userInfo valueForKey:@"name"];
            aUser.gender = [userInfo valueForKey:@"gender"];
            aUser.age = [userInfo valueForKey:@"age"];
            aUser.weight = [userInfo valueForKey:@"weight"];
            aUser.height = [userInfo valueForKey:@"height"];
            aUser.email = [userInfo valueForKey:@"email"];
            
            [[NSUserDefaults standardUserDefaults] setObject:[userInfo valueForKey:@"token"] forKey:@"token"];
            [self.delegate serverCommunicatorLoginSuccess:self];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate serverCommunicator:self loginFailWithMessage:@"Server error"];
    }];
}

- (void)logout
{
    NSDictionary *parameter = @{@"t": @4,
                                @"email": [SRTUser currentUser].email};
    [self.requestManager POST:self.requestURLString parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [SRTUser currentUser].isCurrent = [NSNumber numberWithBool:NO];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"token"];
        [[NSNotificationCenter defaultCenter] postNotificationName:SRTLogoutNotification object:self];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [SRTUser currentUser].isCurrent = [NSNumber numberWithBool:NO];
        [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"token"];
        [[NSNotificationCenter defaultCenter] postNotificationName:SRTLogoutNotification object:self];
    }];
    

}

- (void)uploadCoachingDataForUser:(SRTUser *)user
{
    NSArray *allCoaching = [[SRTUser currentUser].userCoaching array];
    for (SRTCoaching *coaching in allCoaching) {
        if (![coaching.uploaded boolValue]) {
            NSDictionary *dic = @{@"date": coaching.date,
                                  @"length": coaching.length,
                                  @"depth": coaching.depth,
                                  @"rate": coaching.rate,
                                  @"level": coaching.relaxationLevel};
            
            coaching.uploaded = [NSNumber numberWithBool:YES];
            
            NSDictionary *parameter = @{@"t": @5,
                                        @"email": user.email,
                                        @"coaching": dic};
            
            [self.requestManager POST:self.requestURLString parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
                NSDictionary *response = responseObject;
                if (![[response valueForKey:@"code"] isEqualToNumber:@200]) {
                    [self.delegate serverCommunicator:self uploadCoachingDataFailWithMessage:[response valueForKey:@"message"]];
                } else {
                    [self.delegate serverCommunicatorUploadCoachingDataSuccess:self];
                }
            } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
                [self.delegate serverCommunicator:self uploadCoachingDataFailWithMessage:@"Server error"];
            }];

        }
    }
}

- (void)downloadCoachingDataForUser:(SRTUser *)user
{
    NSDictionary *parameter = @{@"t": @6,
                                @"email": user.email};
    [self.requestManager POST:self.requestURLString parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *response = responseObject;
        if (![[response valueForKey:@"code"] isEqualToNumber:@200]) {
            [self.delegate serverCommunicator:self downloadCoachingDataFailWithMessage:[response valueForKey:@"message"]];
        } else {
            NSArray *downloadCoaching = [response valueForKey:@"data"];
            for (NSDictionary *aDic in downloadCoaching) {
                SRTCoaching *aCoaching = [NSEntityDescription insertNewObjectForEntityForName:@"SRTCoaching" inManagedObjectContext:[SRTCoreDataModel sharedCoreDataModel].managedObjectContext];
                aCoaching.coachingUser = user;
                aCoaching.length = [aDic valueForKey:@"length"];
                
                NSDateFormatter *df = [[NSDateFormatter alloc] init];
                df.dateFormat = @"yyyy-MM-dd HH:mm:ss";
                NSDate *date = [df dateFromString:[aDic valueForKey:@"date"]];
                aCoaching.date = date;
                aCoaching.rate = [aDic valueForKey:@"rate"];
                aCoaching.depth = [aDic valueForKey:@"depth"];
                aCoaching.relaxationLevel = [aDic valueForKey:@"level"];
                aCoaching.uploaded = [NSNumber numberWithBool:YES];
            }
        }
        [self.delegate serverCommunicatorDownloadCoachingDataSuccess:self];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate serverCommunicator:self downloadCoachingDataFailWithMessage:@"Server error"];
    }];
}

- (void)updateProfileForUser:(SRTUser *)user withEncryptPassword:(NSString *)encryptPassword;
{
    NSDictionary *parameter = @{@"t": @3,
                                @"email": user.email,
                                @"name": user.name,
                                @"gender": user.gender,
                                @"weight": user.weight,
                                @"height": user.height,
                                @"age": user.age,
                                @"password": encryptPassword};
    
    [self.requestManager POST:self.requestURLString parameters:parameter success:^(AFHTTPRequestOperation *operation, id responseObject) {
        NSDictionary *response = responseObject;
        if (![[response valueForKey:@"code"] isEqualToNumber:@200]) {
            [self.delegate serverCommunicator:self updateProfileFailWithMessage:[response valueForKey:@"message"]];
        } else {
            [self.delegate serverCommunicatorUpdateProfileSuccess:self];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [self.delegate serverCommunicator:self updateProfileFailWithMessage:@"Server error"];
    }];
}

@end
