//
//  SRTDataAnalyser.m
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/3/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTDataAnalyser.h"

@implementation SRTDataAnalyser

#pragma mark - Interface implementation

- (void)startAnalysing
{
    if (self.beltData) {
        
        //too few data
        if ([self.beltData count] <= 1) {
            [self.delegate dataAnalyser:self averageBreathRate:0 averageBreathDepth:0];
            return;
        }
        
        NSMutableArray *delta = [[NSMutableArray alloc] init];
        [delta addObject:@0];
        for (int i = 1; i < [self.beltData count]; i++) {
            float a = [[self.beltData objectAtIndex:i-1] floatValue];
            float b = [[self.beltData objectAtIndex:i] floatValue];
            [delta addObject:[NSNumber numberWithFloat:b-a]];
        }
        
        NSMutableArray *inPoints = [[NSMutableArray alloc] init];
        NSMutableArray *outPoints = [[NSMutableArray alloc] init];
        int over0 = 1;
        int breathCount = 0;

        for (int i = 1; i < [delta count]; i++) {
            float a = [[delta objectAtIndex:i-1] floatValue];
            float b = [[delta objectAtIndex:i] floatValue];
            if (a < 0 && b > 0) {
                [outPoints addObject:[self.beltData objectAtIndex:i]];
                over0++;
            }
            if (a > 0 && b < 0) {
                [inPoints addObject:[self.beltData objectAtIndex:i]];
                over0++;
            }
            
            if (over0 == 3) {
                breathCount++;
                over0 = 1;
            }
        }
        
        float averageRate = (float)breathCount / [self.beltData count] * 60;
        
//        float sum = 0;
//        for (NSNumber *n in inPoints) {
//            sum += [n floatValue];
//        }
//        for (NSNumber *n in outPoints) {
//            sum += - [n floatValue];
//        }
//        
//        float averageDepth = sum / ([inPoints count] + [outPoints count]);
        //mock data because don't know algorithm
        float averageDepth = 100;
        [self.delegate dataAnalyser:self averageBreathRate:averageRate averageBreathDepth:averageDepth];
    }
    
}

@end
