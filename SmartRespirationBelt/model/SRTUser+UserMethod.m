//
//  SRTUser+UserMethod.m
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/4/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTUser+UserMethod.h"
#import "SRTCoreDataModel.h"

@implementation SRTUser (UserMethod)

#pragma mark - Interface implementation

+ (SRTUser *)currentUser
{
    NSManagedObjectContext *context = [SRTCoreDataModel sharedCoreDataModel].managedObjectContext;
    NSFetchRequest *request = [[NSFetchRequest alloc] init];
    NSEntityDescription *description = [NSEntityDescription entityForName:@"SRTUser" inManagedObjectContext:context];
    request.entity = description;
    request.predicate = [NSPredicate predicateWithFormat:@"isCurrent == TRUE"];
    
    NSArray *fetchedObjects = [context executeFetchRequest:request error:NULL];
    
    return [fetchedObjects firstObject];
    
}
@end
