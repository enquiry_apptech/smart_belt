//
//  SRTBelt.m
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 5/28/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTBelt.h"

NSString * const SRTBluetoothFailNotification = @"SRTBluetoothFailNotification";
NSString * const SRTDiscoverBeltNotification = @"SRTDiscoverBeltNotification";
NSString * const SRTConnectBeltNotification = @"SRTConnectBeltNotification";
NSString * const SRTUnrecognizedDeviceNotification = @"SRTUnrecognizedDeviceNotification";
NSString * const SRTDisconnectBeltNotification = @"SRTDisconnectBeltNotification";

@interface SRTBelt ()<CBCentralManagerDelegate, CBPeripheralDelegate>

@property (strong, nonatomic, readwrite) NSNumber *battery;
@property (strong, atomic, readwrite) NSArray *resisterValues; //of NSNumber (unsignd short)

@property (strong, readonly) CBUUID *serviceUUID;
@property (strong, readonly) CBUUID *batteryUUID;
@property (strong, readonly) CBUUID *resisterUUID;
@property (strong, nonatomic) CBCentralManager *beltManager;
@property (strong, nonatomic) CBPeripheral *belt;

@property (strong, nonatomic) CBCharacteristic *batteryCharacteristic;
@property (strong, nonatomic) CBCharacteristic *resisterCharacteristic;

@end

@implementation SRTBelt

#pragma mark - Property getters and setters

- (CBUUID *)serviceUUID
{
    return [CBUUID UUIDWithString:@"00008001-0000-1000-8000-00805F9B34FB"];
}

- (CBUUID *)batteryUUID
{
    return [CBUUID UUIDWithString:@"2A19"];
}

- (CBUUID *)resisterUUID
{
    return [CBUUID UUIDWithString:@"8002"];
}

#pragma mark - Interface implementation

+ (instancetype)sharedBelt
{
    static SRTBelt *singleton = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        singleton = [[SRTBelt alloc] init];
        singleton.connected = NO;
        singleton.beltManager = [[CBCentralManager alloc] initWithDelegate:singleton queue:nil];
    });
    
    return singleton;
}

- (void)startScan
{
    [self.beltManager scanForPeripheralsWithServices:nil options:nil];
}

- (void)stopScan
{
    [self.beltManager stopScan];
}

- (void)connectToPeripheral:(CBPeripheral *)peripheral
{
    [self.beltManager connectPeripheral:peripheral options:nil];
}

- (void)disconnect
{
    [self.beltManager cancelPeripheralConnection:self.belt];
}

- (void)readBattery
{
    [self.belt readValueForCharacteristic:self.batteryCharacteristic];
}

- (void)startUpdateResister
{
    [self.belt setNotifyValue:YES forCharacteristic:self.resisterCharacteristic];
}

- (void)stopUpdateResister
{
    [self.belt setNotifyValue:NO forCharacteristic:self.resisterCharacteristic];
}

#pragma mark - CBCentralManagerDelegate methods

-(void)centralManagerDidUpdateState:(CBCentralManager *)central
{
    self.beltManagerState = central.state;
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI
{
    [[NSNotificationCenter defaultCenter] postNotificationName:SRTDiscoverBeltNotification object:self userInfo:@{@"peripheral":peripheral}];
    
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error
{
    self.connected = NO;
    self.belt = nil;
    [[NSNotificationCenter defaultCenter] postNotificationName:SRTDisconnectBeltNotification object:self];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral
{
    self.belt = peripheral;
    peripheral.delegate = self;
    [peripheral discoverServices:@[self.serviceUUID]];
}

#pragma mark - CBPeripheralDelegate methods

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error
{
    if (error || [peripheral.services count] == 0) {
        [[NSNotificationCenter defaultCenter] postNotificationName:SRTUnrecognizedDeviceNotification object:self];
        [self.beltManager cancelPeripheralConnection:self.belt];
        return;
    }
    self.connected = YES;
    [peripheral discoverCharacteristics:@[self.batteryUUID, self.resisterUUID] forService:peripheral.services[0]];
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error
{
    if (error) {
        [[NSNotificationCenter defaultCenter] postNotificationName:SRTUnrecognizedDeviceNotification object:self];
        [self.beltManager cancelPeripheralConnection:self.belt];
        self.connected = NO;
        return;
    }
    
    for (CBCharacteristic *characteristic in service.characteristics) {
        if ([[characteristic.UUID UUIDString] isEqualToString:[self.batteryUUID UUIDString]]) {
            self.batteryCharacteristic = characteristic;
        } else if ([[characteristic.UUID UUIDString] isEqualToString:[self.resisterUUID UUIDString]]) {
            self.resisterCharacteristic = characteristic;
        }
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SRTConnectBeltNotification object:self];
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error
{
    if (characteristic == self.batteryCharacteristic) {
        NSData *data = characteristic.value;
        NSUInteger value = ((Byte *)[data bytes])[0];
        self.battery = [NSNumber numberWithUnsignedInteger:value];
    } else if (characteristic == self.resisterCharacteristic) {
        NSData *data = characteristic.value;
        self.resisterValues = [self arrayWithResisterData:data];
    }
}

#pragma mark - Helpers

- (NSArray *)arrayWithResisterData:(NSData *)data
{
    NSMutableArray *array = [NSMutableArray array];
    for (int i = 0; i < data.length; i += 2) {
        NSRange dataRange = NSMakeRange(i, 2);
        NSData *subdata = [data subdataWithRange:dataRange];
        
        //for each element, LSB in the first byte, MSB is the last byte
        Byte *bytes = (Byte *)[subdata bytes];
        unsigned short value = (bytes[1] << 8) + bytes[0];
        
        [array addObject:[NSNumber numberWithUnsignedShort:value]];
    }
    return array;
}

@end
















