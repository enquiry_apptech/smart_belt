//
//  SRTAppDelegate.h
//  SmartRespirationBelt
//
//  Created by ENYAN HUANG on 5/15/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SRTAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
