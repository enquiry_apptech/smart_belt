//
//  SRTGraphView.m
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/3/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTGraphView.h"

@implementation SRTGraphView

#pragma mark - Property getters and setters

- (void)setPoints:(NSArray *)points
{
    _points = points;
    [self setNeedsDisplay];
}

#pragma mark - Override

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
    [[UIImage imageNamed:@"graphBg"] drawAtPoint:CGPointMake(0, 0)];
    
    float max = [[self.points valueForKeyPath:@"@max.self"] floatValue] + 1;
    float min = [[self.points valueForKeyPath:@"@min.self"] floatValue];
    CGRect graphRect = CGRectMake(40, 20, rect.size.width - 40, rect.size.height - 20);
    float d = (float)graphRect.size.width / [self.points count] + 1;
    [[UIColor redColor] setStroke];
    UIBezierPath *aPath = [UIBezierPath bezierPath];
    aPath.lineWidth = 5;
    float x = 40.0;
    [aPath moveToPoint:CGPointMake(40, graphRect.size.height)];
    for (NSNumber *heightNumber in self.points) {
        float height = [heightNumber floatValue];
        x += d;
        CGPoint p = CGPointMake(x, (graphRect.size.height - (height - min) / (max - min) * graphRect.size.height));
        [aPath addLineToPoint:p];
    }
    [aPath stroke];
    
}
@end
