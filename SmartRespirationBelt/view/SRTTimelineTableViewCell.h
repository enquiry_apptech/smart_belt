//
//  SRTTimelineTableViewCell.h
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/5/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SRTCoaching;

@interface SRTTimelineTableViewCell : UITableViewCell

@property (strong, nonatomic) SRTCoaching *coachingData;

@end
