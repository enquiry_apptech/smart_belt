//
//  SRTTimelineTableViewCell.m
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/5/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import "SRTTimelineTableViewCell.h"
#import "SRTCoaching.h"

@interface SRTTimelineTableViewCell()

@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthLabel;
@property (weak, nonatomic) IBOutlet UILabel *yearLabel;
@property (weak, nonatomic) IBOutlet UILabel *levelLabel;
@property (weak, nonatomic) IBOutlet UILabel *lengthLabel;
@property (weak, nonatomic) IBOutlet UILabel *rateLabel;
@property (weak, nonatomic) IBOutlet UILabel *depthLabel;
@property (weak, nonatomic) IBOutlet UIImageView *background;

@end

@implementation SRTTimelineTableViewCell

#pragma mark - Property getters and setters

- (void)setCoachingData:(SRTCoaching *)coachingData
{

    NSDateFormatter *df = [[NSDateFormatter alloc] init];
    df.dateFormat = @"dd";
    self.dayLabel.text = [df stringFromDate:coachingData.date];
    df.dateFormat = @"MMM";
    self.monthLabel.text = [df stringFromDate:coachingData.date];
    df.dateFormat = @"yyyy";
    self.yearLabel.text = [df stringFromDate:coachingData.date];
    self.levelLabel.text = coachingData.relaxationLevel;
    self.lengthLabel.text = [NSString stringWithFormat:@"%d mins", [coachingData.length intValue]/60];
    self.rateLabel.text = [NSString stringWithFormat:@"%d/min", [coachingData.rate intValue]];
    self.depthLabel.text = [NSString stringWithFormat:@"%.1f", [coachingData.depth floatValue]];
}

#pragma mark - Override

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    if (selected) {
        self.background.image = [UIImage imageNamed:@"timelinecell_select"];
    } else {
        self.background.image = [UIImage imageNamed:@"timelinecell"];
    }
}

@end
