//
//  SRTUtility.h
//  SmartRespirationBelt
//
//  Created by Enyan Huang on 6/10/14.
//  Copyright (c) 2014 Application Technology Company Limited. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SRTUtility : NSObject

+ (NSString *)encryptPassword:(NSString *)oriPassword;

@end
